import { IDiff } from './iDiff';

export interface IResult
{
    testCaseName:string;
    diff:IDiff[];
    readonly isPassed: boolean;
};