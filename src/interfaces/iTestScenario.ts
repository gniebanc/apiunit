import { Dictionary } from 'typescript-collections';

import { ITestSuite } from './iTestSuite';

export interface ITestScenario {
    name: string;
    testSuites: Dictionary<string, ITestSuite>;
    input: string;
    requestDirName: string;
    responseDirName: string;
    paramsFile: string;
    beforeEachCommand: string;
    scenarios: string[];
    filePrefix: string;
}