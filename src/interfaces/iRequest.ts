import { IActualResponse } from './iActualResponse';
import { ITestCase } from './iTestCase';

export interface IRequest
{
    testCase:ITestCase;
    requestPath:string;
    shouldRetried:number;
    readonly rawRequest:string;
    execute(verbose:boolean): Promise<IActualResponse>;
};