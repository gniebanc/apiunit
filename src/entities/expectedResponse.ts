import * as fs from 'fs';
import { IResponse, Response } from 'http-file';

import { IExpectedResponse } from '../interfaces/iExpectedResponse';
import { ITestCase } from '../interfaces/iTestCase';
import { injectDynamicParameters, injectParameters } from './helpers';

export class ExpectedResponse implements IExpectedResponse
{
    protected _response:IResponse;
    protected get response():IResponse
    {
        return this._response || (this._response = Response.parse(fs.readFileSync(this.responsePath, 'utf8')));
    }

    get statusCode():number { return this.response.statusCode; }
    get headers():object { return this.response.headers; }
    get body():any  
    { 
        let result = injectParameters(this.response.body, this.testCase.suite.parameters);
        result = injectDynamicParameters(this.testCase.suite.testCases, result);
        return result; 
    }
    responsePath:string;
    testCase:ITestCase;

    constructor(path:string)
    {
        this.responsePath = path;
    }
};