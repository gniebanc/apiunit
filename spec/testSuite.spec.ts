import { assert } from 'chai';

import { TestCase } from '../src/entities/testCase';
import { TestSuite } from '../src/entities/testSuite';

describe('TestSuite', () => 
{ 
    it('Load test cases from a directory properly', async () => 
    {
        let suite = new TestSuite('./assets/testSuite/requests', './assets/testSuite/responses');
        
        assert.equal(suite.testCases.size(), 2);

        assert.instanceOf(suite.testCases.getValue('request1'), TestCase);
        assert.instanceOf(suite.testCases.getValue('request2'), TestCase);
        assert.isUndefined(suite.testCases.getValue('invalid'));

        let testCase1 = suite.testCases.getValue('request1');
        assert.equal(testCase1.actualResponse, undefined);
        assert.equal(testCase1.expectedResponseFilePath, 'assets/testSuite/responses/request1.resp');
        assert.equal(testCase1.requestFilePath, 'assets/testSuite/requests/request1.req');

        let testCase2 = suite.testCases.getValue('request2');
        assert.equal(testCase2.actualResponse, undefined);
        assert.equal(testCase2.expectedResponseFilePath, 'assets/testSuite/responses/request2.msg');
        assert.equal(testCase2.requestFilePath, 'assets/testSuite/requests/request2.msg');
    });

    it('Load a test case from a file properly', async () => 
    {
        let suite = new TestSuite('./assets/testSuite/requests/request1.req', './assets/testSuite/responses');
        
        assert.equal(suite.testCases.size(), 1);

        assert.instanceOf(suite.testCases.getValue('request1'), TestCase);
        assert.isUndefined(suite.testCases.getValue('invalid'));
    });

    it('Handle a test case loading failure properly', async () => 
    {
        assert.throws(()=>new TestSuite('./assets/testSuite/requests/invalid.req', './assets/testSuite/responses'), './assets/testSuite/requests/invalid.req does not exists.');
    });

    it('Handle test cases loading failure properly', async () => 
    {
        assert.throws(()=>new TestSuite('./assets/testSuite/invalid', './assets/testSuite/responses'), './assets/testSuite/invalid does not exists.');
    });
});