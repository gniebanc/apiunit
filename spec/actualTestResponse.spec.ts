import { assert } from 'chai';
import { Mock } from 'typemoq';
import { gzipSync } from 'zlib';

import { ActualResponse } from '../src/entities/actualResponse';
import { IExpectedResponse } from '../src/interfaces/iExpectedResponse';


class FakeResponse extends ActualResponse
{
    constructor(statusCode:number, headers:object = {}, body:any = undefined)
    {
        super(statusCode, headers, body);
    }
}

function mockResponse(statusCode:number, headers:object = {}, body:any = undefined): IExpectedResponse
{
    let mockedExpectedResponse = Mock.ofType<IExpectedResponse>();
    mockedExpectedResponse.setup((r)=>r.statusCode).returns(()=>statusCode);
    mockedExpectedResponse.setup((r)=>r.headers).returns(()=>headers);
    mockedExpectedResponse.setup((r)=>r.body).returns(()=>body);

    return mockedExpectedResponse.object;
}
describe('actualResponse', () =>
{
    it('diff - statusCode - passed', async () =>
    {
        let response = new FakeResponse(200);
        let result = response.diff(mockResponse(200));

        assert.equal(result.length, 0);
    });

    it('diff - statusCode - passed', async () =>
    {
        let response = new FakeResponse(204);
        let result = response.diff(mockResponse(204));

        assert.equal(result.length, 0);
    });

    it('diff - statusCode - failed', async () =>
    {
        let response = new FakeResponse(204);
        let result = response.diff(mockResponse(200));

        assert.equal(result.length, 1);
        assert.equal(result[0].message, "Status code assertion failed.  Expected '200' to equal to '204'.");
    });

    it('diff - headers - passed', async () =>
    {
        let response = new FakeResponse(undefined, {'X-Header-1': 'X-Value-1', 'X-Header-2': 'X-Value-2'});
        let result = response.diff(mockResponse(undefined, {'X-Header-2': 'X-Value-2', 'X-Header-1': 'X-Value-1'}));

        assert.equal(result.length, 0);
    });

    it('diff - headers - passed - even with one extra header', async () =>
    {
        let response = new FakeResponse(undefined, {'X-Header-1': 'X-Value-1', 'X-Header-2': 'X-Value-2'});
        let result = response.diff(mockResponse(undefined, {'X-Header-1': 'X-Value-1'}));

        assert.equal(result.length, 0);
    });

    it('diff - headers - failed - missing one header', async () =>
    {
        let response = new FakeResponse(undefined, {'X-Header-1': 'X-Value-1', 'X-Header-2': 'X-Value-2'});
        let result = response.diff(mockResponse(undefined, {'X-Header-2': 'X-Value-2', 'X-Header-1': 'X-Value-1', 'X-Header-3': 'X-Value-3'}));

        assert.equal(result.length, 1);
        assert.equal(result[0].message, "Header assertion failed. Expected header 'X-Header-3' is missing from the response.");
    });

    it('diff - binary body - passed', async () =>
    {
        let response = new FakeResponse(undefined, undefined, Buffer.from('Hello, world!'));
        let result = response.diff(mockResponse(undefined, undefined, 'Hello, world!'));

        assert.equal(result.length, 0);
    });

    it('diff - body - passed', async () =>
    {
        let response = new FakeResponse(undefined, undefined, 'Hello, world!');
        let result = response.diff(mockResponse(undefined, undefined, 'Hello, world!'));

        assert.equal(result.length, 0);
    });

    it('diff - body - passed', async () =>
    {
        let response = new FakeResponse(undefined, undefined, 'Hello, world!');
        let result = response.diff(mockResponse(undefined, undefined, 'Hi, world!'));

        assert.equal(result.length, 1);
        assert.equal(result[0].message, "Body assertion failed. Expected 'Hi, world!' to equal to 'Hello, world!'");
    });

    it('diff - json body - passed', async () =>
    {
        let response = new FakeResponse(undefined, {'content-type': 'application/json'}, '{"name": "world", "action": "hello"}');
        let result = response.diff(mockResponse(undefined, undefined, '{"name": "world", "action": "hello"}'));
        assert.equal(result.length, 0);
    });

    it('diff - json body - passed - content-type header includes charset', async () =>
    {
        let response = new FakeResponse(undefined, {'content-type': 'application/json; charset=UTF-8'}, '{"name":"world","action":"hello"}');
        let result = response.diff(mockResponse(undefined, undefined, '{"name": "world", "action": "hello"}'));
        assert.equal(result.length, 0);
    });


    it('diff - json body - failed - a property is missing.', async () =>
    {
        let response = new FakeResponse(undefined, {'content-type': 'application/json'}, '{"name": "world"}');
        let result = response.diff(mockResponse(undefined, undefined, '{"name": "world", "action": "hello"}'));

        assert.equal(result.length, 1);
        assert.equal(result[0].message, "Body assertion failed at '$.action'. Expected 'hello' but it is missing.'");
    });

    it('diff - json body - failed - an object is missing.', async () =>
    {
        let response = new FakeResponse(undefined, {'content-type': 'application/json'}, '{"name": "world"}');
        let result = response.diff(mockResponse(undefined, undefined, '{"name": "world", "action": {"do": "something"}}'));

        assert.equal(result.length, 1);
        //This would normally says expected [Object object] to equal [Object object]
        assert.equal(result[0].message, "Body assertion failed at '$.action'. Expected '{\"do\":\"something\"}' but it is missing.'");
    });

    it('diff - json body - failed - a property is missing.', async () =>
    {
        let response = new FakeResponse(undefined, {'content-type': 'application/json'}, '{}');
        let result = response.diff(mockResponse(undefined, undefined, '{ "items": [{"name": "world"}]}'));

        assert.equal(result.length, 1);
        assert.equal(result[0].message, "Body assertion failed at '$.items'. Expected '[{\"name\":\"world\"}]' but it is missing.'");
    });

    it('diff - json body - failed - missing array item.', async () =>
    {
        let response = new FakeResponse(undefined, {'content-type': 'application/json'}, '{ "items": [{"name": "world"},{"name":"haha"}]}');
        let result = response.diff(mockResponse(undefined, undefined, '{ "items": [{"name": "world"}]}'));

        assert.equal(result.length, 1);
        assert.equal(result[0].message, "Body assertion failed at '$.items[2]'. An array item is missing. '{\"name\":\"haha\"}' does not exist in the response.");
    });


    [
        {
            name: "diff - json body - failed - any array order when true - mismatching data and first level in order",
            arrayOrderHeader: "true",
            fakeResponseData:  [
                {
                    "key1": "a"
                },
                {
                    "items1":["a","c",{"b":1}],
                    "items2":["z","x","w"],
                }
            ],
            expectedResponseData: [
                {
                    "key1": "b"
                },
                {
                    "items1":[{"b":1},"a", "d"],
                    "items2":["x","w","z"],
                }
            ],
            expectedResult: [
                { message: "Body assertion failed at '$.data,1,items1,2'. Expected 'd' to equal to 'c'" },
                { message: "Body assertion failed at '$.data,0,key1'. Expected 'b' to equal to 'a'" },
            ]
        },
        {
            name: "diff - json body - failed - any array order - mismatching data and first level in order",
            arrayOrderHeader: "$..*",
            fakeResponseData:  [
                {
                    "key1": "a"
                },
                {
                    "items1":["a","c",{"b":1}],
                    "items2":["z","x","w"],
                }
            ],
            expectedResponseData: [
                {
                    "key1": "b"
                },
                {
                    "items1":[{"b":1},"a", "d"],
                    "items2":["x","w","z"],
                }
            ],
            expectedResult: [
                { message: "Body assertion failed at '$.data,1,items1,2'. Expected 'd' to equal to 'c'" },
                { message: "Body assertion failed at '$.data,0,key1'. Expected 'b' to equal to 'a'" },
            ]
        },
        {
            name: "diff - json body - failed - any array order - mismatching data",
            arrayOrderHeader: "$..*",
            fakeResponseData:  [
                {
                    "key1": "a"
                },
                {
                    "items1":["a","c",{"b":1}],
                    "items2":["z","x","w"],
                }
            ],
            expectedResponseData: [
                {
                    "items1":[{"b":1},"a", "d"],
                    "items2":["x","w","z"],
                },
                {
                    "key1": "b"
                }
            ],
            expectedResult: [
                { message: `Body assertion failed at '$.data,1,items1'. Expected 'undefined' to equal to '["a","c",{"b":1}]'` },
                { message: `Body assertion failed at '$.data,1,items2'. Expected 'undefined' to equal to '["z","x","w"]'` },
                { message: "Body assertion failed at '$.data,1,key1'. Expected 'b' but it is missing.'" },
                { message: `Body assertion failed at '$.data,0,key1'. Expected 'undefined' to equal to 'a'` },
                { message: `Body assertion failed at '$.data,0,items1'. Expected '[{"b":1},"a","d"]' but it is missing.'`},
                { message: `Body assertion failed at '$.data,0,items2'. Expected '["x","w","z"]' but it is missing.'`},
            ]
        },
        {
            name: "diff - json body - failed - any array order",
            arrayOrderHeader: "$..*",
            fakeResponseData:  [
                {
                    "items1":["a","c",{"b":1}],
                    "items2":["z","x","w"],
                },
                {
                    "key1": "a"
                }
            ],
            expectedResponseData: [
                {
                    "key1": "b"
                },
                {
                    "items1":[{"b":1},"a", "c"],
                    "items2":["x","w","z"],
                }
            ],
            expectedResult: [
                { message: "Body assertion failed at '$.data,0,key1'. Expected 'b' to equal to 'a'" },
            ]
        },
        {
            name: "diff - json body - failed - any array order and too few items",
            arrayOrderHeader: "$..*",
            fakeResponseData:  [
                {
                    "items1":["a","c",{"b":1}],
                    "items2":["z","x","w"],
                },
                {
                    "key1": "a"
                }
            ],
            expectedResponseData: [
                {
                    "key1": "b"
                },
                {
                    "items1":[{"b":1},"a", "c"],
                    "items2":["x","w","z"],
                },
                {
                    "key1": "c"
                }
            ],
            expectedResult: [
                { message: "Body assertion failed at '$.data[3]'. An array item is missing. Expected '{\"key1\":\"c\"}' to equal to 'undefined'" },
                { message: "Body assertion failed at '$.data,0,key1'. Expected 'b' to equal to 'a'" },
            ]
        },
        {
            name: "diff - json body - failed - any array order and too many items",
            arrayOrderHeader: "$..*",
            fakeResponseData:  [
                {
                    "items1":["a","c",{"b":1}],
                    "items2":["z","x","w"],
                },
                {
                    "key1": "a"
                },
                {
                    "key1": "c"
                }
            ],
            expectedResponseData: [
                {
                    "key1": "b"
                },
                {
                    "items1":[{"b":1},"a", "c"],
                    "items2":["x","w","z"],
                }
            ],
            expectedResult: [
                { message: "Body assertion failed at '$.data[3]'. An array item is missing. '{\"key1\":\"c\"}' does not exist in the response."},
                { message: "Body assertion failed at '$.data,0,key1'. Expected 'b' to equal to 'a'" },
            ]
        },
        {
            name: "diff - json body - failed - only root path allows any order",
            arrayOrderHeader: "$.data",
            fakeResponseData:  [
                {
                    "items1":["a","c",{"b":1}],
                    "items2":["z","x","w"],
                },
                {
                    "key1": "a"
                }
            ],
            expectedResponseData: [
                {
                    "key1": "a"
                },
                {
                    "items1":[{"b":1},"a", "c"],
                    "items2":["x","w","z"],
                }
            ],
            expectedResult: [
                { message: `Body assertion failed at '$.data,1,items1,2'. Expected 'c' to equal to '{"b":1}'` },
                { message: "Body assertion failed at '$.data,1,items1,1'. Expected 'a' to equal to 'c'" },
                { message: `Body assertion failed at '$.data,1,items1,0'. Expected '{"b":1}' to equal to 'a'` },
                { message: "Body assertion failed at '$.data,1,items2,2'. Expected 'z' to equal to 'w'" },
                { message: "Body assertion failed at '$.data,1,items2,1'. Expected 'w' to equal to 'x'" },
                { message: "Body assertion failed at '$.data,1,items2,0'. Expected 'x' to equal to 'z'" },
            ]
        },
        {
            name: "diff - json body - failed - multiple paths allow any order",
            arrayOrderHeader: "$.data,$.data[*].items1",
            fakeResponseData:  [
                {
                    "items1":["a","c",{"b":1}],
                    "items2":["z","x","w"],
                },
                {
                    "key1": "a"
                }
            ],
            expectedResponseData: [
                {
                    "key1": "a"
                },
                {
                    "items1":[{"b":1},"a", "c"],
                    "items2":["x","w","z"],
                }
            ],
            expectedResult: [
                { message: "Body assertion failed at '$.data,1,items2,2'. Expected 'z' to equal to 'w'" },
                { message: "Body assertion failed at '$.data,1,items2,1'. Expected 'w' to equal to 'x'" },
                { message: "Body assertion failed at '$.data,1,items2,0'. Expected 'x' to equal to 'z'" },
            ]
        },
        {
            name: "diff - json body - failed - mixed strict and any array order - first level not in order",
            arrayOrderHeader: "$.data[*].items1",
            fakeResponseData:  [
                {
                    "items1":[{"b":1},"a", "d"],
                    "items2":["x","w","z"],
                },
                {
                    "key1": "b"
                }
            ],
            expectedResponseData: [
                {
                    "key1": "b"
                },
                {
                    "items1":[{"b":1},"a", "d"],
                    "items2":["x","w","z"],
                }
            ],
            expectedResult: [
                { message: "Body assertion failed at '$.data,1,key1'. Expected 'undefined' to equal to 'b'" },
                { message: `Body assertion failed at '$.data,1,items1'. Expected '[{"b":1},"a","d"]' but it is missing.'`},
                { message: `Body assertion failed at '$.data,1,items2'. Expected '["x","w","z"]' but it is missing.'`},
                { message: `Body assertion failed at '$.data,0,items1'. Expected 'undefined' to equal to '[{"b":1},"a","d"]'` },
                { message: `Body assertion failed at '$.data,0,items2'. Expected 'undefined' to equal to '["x","w","z"]'` },
                { message: `Body assertion failed at '$.data,0,key1'. Expected 'b' but it is missing.'` },
            ]
        },
        {
            name: "diff - json body - failed - mixed strict and any array order - first level in order",
            arrayOrderHeader: "$.data[*].items1",
            fakeResponseData:  [
                {
                    "key1": "a"
                },
                {
                    "items1":["a","c",{"b":1}],
                    "items2":["z","x","w"],
                }
            ],
            expectedResponseData: [
                {
                    "key1": "b"
                },
                {
                    "items1":[{"b":1},"a", "d"],
                    "items2":["x","w","z"],
                }
            ],
            expectedResult: [
                { message: "Body assertion failed at '$.data,1,items1,2'. Expected 'd' to equal to 'c'" },
                { message: "Body assertion failed at '$.data,1,items2,2'. Expected 'z' to equal to 'w'" },
                { message: "Body assertion failed at '$.data,1,items2,1'. Expected 'w' to equal to 'x'" },
                { message: "Body assertion failed at '$.data,1,items2,0'. Expected 'x' to equal to 'z'" },
                { message: "Body assertion failed at '$.data,0,key1'. Expected 'b' to equal to 'a'" },
            ]
        },
        {
            name: "diff - json body - failed - default strict array order",
            arrayOrderHeader: undefined,
            fakeResponseData:  [
                {
                    "key1": "a"
                },
                {
                    "items1":["a","c",{"b":1}],
                    "items2":["z","x","w"],
                }
            ],
            expectedResponseData: [
                {
                    "key1": "b"
                },
                {
                    "items1":[{"b":1},"a", "d"],
                    "items2":["x","w","z"],
                }
            ],
            expectedResult: [
                { message: "Body assertion failed at '$.data,1,items1,2'. Expected 'd' to equal to '{\"b\":1}'" },
                { message: "Body assertion failed at '$.data,1,items1,1'. Expected 'a' to equal to 'c'" },
                { message: "Body assertion failed at '$.data,1,items1,0'. Expected '{\"b\":1}' to equal to 'a'" },
                { message: "Body assertion failed at '$.data,1,items2,2'. Expected 'z' to equal to 'w'" },
                { message: "Body assertion failed at '$.data,1,items2,1'. Expected 'w' to equal to 'x'" },
                { message: "Body assertion failed at '$.data,1,items2,0'. Expected 'x' to equal to 'z'" },
                { message: "Body assertion failed at '$.data,0,key1'. Expected 'b' to equal to 'a'" },
            ]
        },
        {
            name: "diff - json body - passed - works with regular expressions as expected values",
            arrayOrderHeader: "true",
            fakeResponseData:  [
                {
                    "key1": "anything-1",
                    "key2": "b",
                    "key3": [
                        {
                            "key4": "anything-2",
                            "key5": "b"
                        },
                        {
                            "key4": "anything-3",
                            "key5": "c"
                        }
                    ]
                },
                {
                    "key1": "anything-4",
                    "key2": "c",
                    "key3": [
                        {
                            "key4": "anything-5",
                            "key5": "b"
                        },
                        {
                            "key4": "anything-6",
                            "key5": "c"
                        }
                    ]
                }
            ],
            expectedResponseData: [
                {
                    "key1": ".+",
                    "key2": "c",
                    "key3": [
                        {
                            "key4": ".+",
                            "key5": "c",
                        },
                        {
                            "key4": ".+",
                            "key5": "b",
                        }
                    ]
                },
                {
                    "key1": ".+",
                    "key2": "b",
                    "key3": [
                        {
                            "key4": ".+",
                            "key5": "c",
                        },
                        {
                            "key4": ".+",
                            "key5": "b",
                        }
                    ]
                }
            ],
            expectedResult: []
        }
    ].forEach(test => {
        it(test.name, async () => {
            const fakeResponse = JSON.stringify({ data: test.fakeResponseData });
            let expectedResponse = JSON.stringify({ data: test.expectedResponseData });
    
            let response = new FakeResponse(undefined, {'content-type': 'application/json'}, fakeResponse);
            let result = response.diff(mockResponse(undefined, { "apiunit-ignore-array-order": test.arrayOrderHeader }, expectedResponse));
    
            assert.deepEqual(result, test.expectedResult);
        });
    });

    [
        'text/xml',
        'application/xml'
    ].forEach(contentType =>
    {
        it('diff - xml body - passed', async () =>
        {
            let response = new FakeResponse(undefined, {'content-type': contentType}, '<Name>world</Name><Action>hello</Action>');
            let result = response.diff(mockResponse(undefined, undefined, '<Name>world</Name><Action>hello</Action>'));
            assert.equal(result.length, 0);
        });

        it('diff - xml body - passed - regexp', async () =>
        {
            let response = new FakeResponse(undefined, {'content-type': contentType}, '<Name>world</Name><Action>hello</Action>');
            let result = response.diff(mockResponse(undefined, undefined, '<Name>worlds?</Name><Action>h(e|a)llo</Action>'));
            assert.equal(result.length, 0);
        });

        it('diff - xml body - failed - a property is missing.', async () =>
        {
            let response = new FakeResponse(undefined, {'content-type': contentType}, '<Name>world</Name>');
            let result = response.diff(mockResponse(undefined, undefined, '<Name>world</Name><Action>hello</Action>'));

            assert.equal(result.length, 1);
            assert.equal(result[0].message, "Body assertion failed at '$.Action'. Expected 'hello' but it is missing.'");
        });

        it('diff - xml body - failed - an object is missing.', async () =>
        {
            let response = new FakeResponse(undefined, {'content-type': contentType}, '<Name>world</Name>');
            let result = response.diff(mockResponse(undefined, undefined, '<Name>world</Name>\n<Action><Do>something</Do></Action>'));

            assert.equal(result.length, 1);
            //This would normally says expected [Object object] to equal [Object object]
            assert.equal(result[0].message, "Body assertion failed at '$.Action'. Expected '{\"Do\":\"something\"}' but it is missing.'");
        });

        it('diff - xml body - failed - string should be array.', async () =>
        {
            let response = new FakeResponse(undefined, {'content-type': contentType}, '<items><name>world</name><name>haha</name></items>');
            // let response = new FakeResponse(undefined, {'content-type': 'text/xml'}, '{ "items": [{"name": "world"},{"name":"haha"}]}');
            let result = response.diff(mockResponse(undefined, undefined, '<items><name>world</name></items>'));

            assert.equal(result.length, 1);
            assert.equal(result[0].message, "Body assertion failed at '$.items,name'. Expected 'world' to equal to '[\"world\",\"haha\"]'");
        });

        it('diff - xml body - failed - missing array item.', async () =>
        {
            let response = new FakeResponse(undefined, {'content-type': contentType}, '<items><name>world</name><name>haha</name><name>thing</name></items>');
            let result = response.diff(mockResponse(undefined, undefined, '<items><name>world</name><name>haha</name></items>'));

            assert.equal(result.length, 1);
            assert.equal(result[0].message, "Body assertion failed at '$.items,name[3]'. An array item is missing. 'thing' does not exist in the response.");
        });

        it('diff - xml body - failed - mismatching array item.', async () =>
        {
            let response = new FakeResponse(undefined, {'content-type': contentType}, '<items><name>world</name><name>ha</name></items>');
            let result = response.diff(mockResponse(undefined, undefined, '<items><name>world</name><name>haha</name></items>'));

            assert.equal(result.length, 1);
            assert.equal(result[0].message, "Body assertion failed at '$.items,name,1'. Expected 'haha' to equal to 'ha'");
        });
    });

    it('diff - gzip body - passed', async () =>
    {
        let response = new FakeResponse(undefined, {'content-type': 'application/gzip' }, gzipSync('Hello, world!'));
        let result = response.diff(mockResponse(undefined, {'content-type': 'application/gzip' }, 'Hello, world!'));

        assert.equal(result.length, 0);
    });

    it('diff - gzip body - failed', async () =>
    {
        let response = new FakeResponse(undefined, {'content-type': 'application/gzip' }, gzipSync('Hello, world!'));
        let result = response.diff(mockResponse(undefined, {'content-type': 'application/gzip' }, 'Hi, world!'));

        assert.equal(result.length, 1);
        assert.equal(result[0].message, "Body assertion failed. Expected 'Hi, world!' to equal to 'Hello, world!'");
    });
});
